document.addEventListener("DOMContentLoaded", () => {
  // Your JavaScript code here

  const toggleButtons = document.querySelectorAll(".toggle-button");
  let activeSliderContent = null;

  toggleButtons.forEach((button) => {
    button.addEventListener("click", () => {
      const sliderContent =
        button.parentElement.querySelector(".slider-content");

      if (activeSliderContent) {
        // Close the previously active slider content
        activeSliderContent.classList.remove("active");
      }

      if (activeSliderContent !== sliderContent) {
        // If the clicked slider content is not the same as the active one, open it
        sliderContent.classList.add("active");
        activeSliderContent = sliderContent;
      } else {
        // If the clicked slider content is the same as the active one, close it
        activeSliderContent = null;
      }

      // Slideshow functionality
      const slideshow = sliderContent.querySelector(".slideshow");
      const slides = slideshow.querySelectorAll(".slide"); // Get all the slides

      let currentIndex = 0;
      const totalSlides = slides.length;

      function showSlide(index) {
        for (let i = 0; i < slides.length; i++) {
          slides[i].style.display = i === index ? "block" : "none";
        }
      }

      function updateSlideNumber() {
        // Update the slide number element with the current slide number
        const slideNumberElement = sliderContent.querySelector(".slide-number");
        slideNumberElement.textContent = `${currentIndex + 1}/${totalSlides}`;
      }

      function nextSlide() {
        currentIndex = (currentIndex + 1) % slides.length;
        showSlide(currentIndex);
        updateSlideNumber();
      }

      function prevSlide() {
        currentIndex = (currentIndex - 1 + slides.length) % slides.length;
        showSlide(currentIndex);
        updateSlideNumber();
      }

      const prevButton = sliderContent.querySelector(".prev-button");
      const nextButton = sliderContent.querySelector(".next-button");

      prevButton.addEventListener("click", () => {
        prevSlide();
      });

      nextButton.addEventListener("click", () => {
        nextSlide();
      });

      // Show the initial slide number and hide all slides except the first one
      updateSlideNumber();
      showSlide(currentIndex);

      // Close button functionality
      const closeButton = sliderContent.querySelector(".close-button");
      closeButton.addEventListener("click", () => {
        activeSliderContent.classList.remove("active");
        activeSliderContent = null;
      });
    });
  });

  let closeButton = document.querySelector(".main-close-button");
  closeButton.addEventListener("click", () => {
    const floating_sidebar = document.querySelector(".highlights");
    floating_sidebar.classList.remove("show");
    floating_sidebar.classList.add("hide");

    const floating_sidebar_open = document.querySelector(".highlights-open");
    floating_sidebar_open.classList.remove("hide");
    floating_sidebar_open.classList.add("show");
  });

  let openButton = document.querySelector(".main-open-button");
  openButton.addEventListener("click", () => {
    const floating_sidebar = document.querySelector(".highlights");
    floating_sidebar.classList.remove("hide");
    floating_sidebar.classList.add("show");    

    const floating_sidebar_open = document.querySelector(".highlights-open");
    floating_sidebar_open.classList.remove("show");
    floating_sidebar_open.classList.add("hide");
  });
});

const accordionButtons = document.querySelectorAll('.usa-accordion__button');

accordionButtons.forEach(button => {
  button.addEventListener('click', function () {
    console.log('Button clicked:', button.textContent);

    const accordionContent = button.closest('.usa-accordion__heading').nextElementSibling;

    // Toggle the 'show' class on the accordion content
    if (accordionContent.classList.contains('show')) {
      accordionContent.classList.remove('show');
      button.setAttribute('aria-expanded', 'false');
    } else {
      accordionContent.classList.add('show');
      button.setAttribute('aria-expanded', 'true');
    }
  });
});